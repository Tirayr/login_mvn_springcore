package am.egs.mavenSpring.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckCookie {

    public static void checkCookie(HttpServletRequest request, HttpServletResponse response){
        String remember = request.getParameter("remember");

        if (remember != null){
            Cookie cookie_1 = new Cookie("email",request.getParameter("email"));
            Cookie cookie_2 = new Cookie("password", request.getParameter("password"));
            cookie_1.setMaxAge(100000);
            cookie_2.setMaxAge(100000);
            response.addCookie(cookie_1);
            response.addCookie(cookie_2);
        }
    }

}
