package am.egs.mavenSpring.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static String url = "jdbc:mysql://localhost:3306/mvnspr";
    private static String username = "root";
    private static String password = "mmtiro";
    private static String driver = "com.mysql.jdbc.Driver";

    private static Connection connection;

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (connection == null || connection.isClosed()){
            Class.forName(driver);
            connection = DriverManager.getConnection(url,username,password);
        }
        return connection;
    }




}
