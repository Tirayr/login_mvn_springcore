package am.egs.mavenSpring.service;

import am.egs.mavenSpring.model.User;
import am.egs.mavenSpring.repository.UserRepositroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Service
public class UserService {

    @Autowired
    private UserRepositroy userRepositroy;

 //   UserRepositroy userRepositroy = new UserRepositroy();

    public void add(User user) throws SQLException, ClassNotFoundException {
        userRepositroy.add(user);
    }

    public User getUser(String email) throws SQLException, ClassNotFoundException {
        User user = userRepositroy.getByEmail(email);
        return user;
    }
}
