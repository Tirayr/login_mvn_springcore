package am.egs.mavenSpring.repository;

import am.egs.mavenSpring.model.User;
import am.egs.mavenSpring.util.DBConnection;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


@Repository
public class UserRepositroy {
    public void add(User user) throws SQLException, ClassNotFoundException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement prstm = connection.prepareStatement("insert into mvnspr.user values (0,?,?,?)");
        prstm.setString(1, user.getName());
        prstm.setString(2, user.getEmail());
        prstm.setString(3, user.getPassword());

        prstm.execute();
        prstm.close();

    }

    public User getByEmail(String email) throws SQLException, ClassNotFoundException {
        User user = null;

        Connection connection = DBConnection.getConnection();
        PreparedStatement prstm = connection.prepareStatement("select * from mvnspr.user where email = ?");
        prstm.setString(1, email);
        ResultSet rs = prstm.executeQuery();

        if (rs.next()) {
            user = new User();
            user.setId(rs.getInt("id"));
            user.setName(rs.getString("name"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
        }

        rs.close();
        prstm.close();

        return user;
    }
}
