package am.egs.mavenSpring.controller;

import am.egs.mavenSpring.controller.config.Config;
import am.egs.mavenSpring.controller.config.SingleConfig;
import am.egs.mavenSpring.model.User;
import am.egs.mavenSpring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet("/register")
@Controller
public class Register extends HttpServlet {

    @Autowired
    private UserService userService;

    @Autowired
    private User user;


    @Override
    public void init() throws ServletException {
   //     ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        user = SingleConfig.getConfig().getBean(User.class);

        userService = SingleConfig.getConfig().getBean(UserService.class);

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

      //   User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);

   //   UserService userService = new UserService();

        try {
            userService.add(user);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        request.getSession().setAttribute("user",user);
        response.sendRedirect("/pages/home.jsp");
    }
}
