package am.egs.mavenSpring.controller.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SingleConfig {

   private static ApplicationContext applicationContext;

    public static ApplicationContext getConfig(){
        if (applicationContext == null){
            applicationContext = new AnnotationConfigApplicationContext(Config.class);
        }
        return applicationContext;
    }

}
