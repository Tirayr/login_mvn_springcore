package am.egs.mavenSpring.controller.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("am.egs.mavenSpring")
public class Config {



}
