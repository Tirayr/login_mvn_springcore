package am.egs.mavenSpring.controller;

import am.egs.mavenSpring.controller.config.Config;
import am.egs.mavenSpring.controller.config.SingleConfig;
import am.egs.mavenSpring.model.User;
import am.egs.mavenSpring.service.UserService;
import am.egs.mavenSpring.util.CheckCookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet("/login")
@Controller
public class Login extends HttpServlet {

    @Autowired
    private UserService userService;

    @Override
    public void init() throws ServletException {
        //ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        userService = SingleConfig.getConfig().getBean(UserService.class);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String email, password;

        if (request.getAttribute("email") != null) {
            email = (String) request.getAttribute("email");
            password = (String) request.getAttribute("password");
        } else {
            email = request.getParameter("email");
            password = request.getParameter("password");
        }

        //   UserService userService = new UserService();

        User user = null;
        try {
            user = userService.getUser(email);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (user == null || !user.getPassword().equals(password)) {
            request.setAttribute("message", "wrong email or password");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        HttpSession session = request.getSession();
        session.setAttribute("user", user);

        CheckCookie.checkCookie(request, response);

        response.sendRedirect("/pages/home.jsp");


    }
}
